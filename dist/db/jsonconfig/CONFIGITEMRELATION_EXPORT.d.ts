declare const _default: {
    'relationId': string;
    'relationType': string;
    'parentItemId': string;
    'childItemId': string;
}[];
export default _default;
//# sourceMappingURL=CONFIGITEMRELATION_EXPORT.d.ts.map