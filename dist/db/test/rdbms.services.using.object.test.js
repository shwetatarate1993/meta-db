"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../constants");
const jsonconfig_1 = require("../jsonconfig");
const metadata_util_1 = require("../../utilities/metadata.util");
const load_config_1 = require("../load.config");
const loadDataUtils = __importStar(require("../load.config"));
const config_data_model_1 = __importDefault(require("../../models/config.data.model"));
const sinon_1 = __importDefault(require("sinon"));
const init_config_1 = require("../../init-config");
const dbClient = metadata_util_1.getAppConfigDBClient(constants_1.MOCK);
const menuId = '00137dff-9f4b-4027-bfb2-bd29cb7ee0d2';
const parentMenuId = 'a17ef20c-0fec-42d9-80d8-0a8cb3fa08bf';
let loadConfigMock;
beforeAll(() => {
    const props = load_config_1.processProperties(jsonconfig_1.configitempropertyjson);
    const rels = load_config_1.processRelations(jsonconfig_1.configitemrelationjson);
    const privs = load_config_1.processPrivileges(jsonconfig_1.configitemprivjson);
    const configObject = new config_data_model_1.default(jsonconfig_1.configitemsjson, props, privs, rels);
    loadConfigMock = sinon_1.default.mock(loadDataUtils);
    loadConfigMock.expects('loadConfigData').returns(configObject);
    init_config_1.MetaDBConfig.configure(constants_1.MOCK);
});
afterAll(() => {
    loadConfigMock.restore();
});
describe('Fetch Services for ConfigItem', async () => {
    test('fetch config item by id', async () => {
        const item = await dbClient.fetchConfigItemById(menuId);
        sinon_1.default.assert.match(item.configObjectType, 'Menu');
        sinon_1.default.assert.match(item.configObjectId, menuId);
    });
    test('fetch config item by type', async () => {
        const configType = 'Language';
        const items = await dbClient.fetchConfigItemByType(configType);
        sinon_1.default.assert.match(items.length > 1, true);
        sinon_1.default.assert.match(items[0].configObjectType, configType);
    });
    test('fetch config items for list of ids', async () => {
        const configIdList = ["00137dff-9f4b-4027-bfb2-bd29cb7ee0d2",
            "00a63dca-c235-4e6c-9323-08a90264a8c3"];
        const items = await dbClient.fetchConfigItemsForIdList(configIdList);
        sinon_1.default.assert.match(configIdList.length, items.length);
    });
});
describe('Fetch Services for ConfigItem Properties', async () => {
    test('fetch properties by config item id', async () => {
        const props = await dbClient.fetchConfigPropsByConfigId(menuId);
        const hrefPropertyCheck = props.find(prop => prop.propertyName === "HREF_BASE_URL");
        sinon_1.default.assert.match(hrefPropertyCheck != undefined, true);
        sinon_1.default.assert.match(props[0].itemId, menuId);
    });
});
describe('Fetch Services for ConfigItemPrivileges', async () => {
    test('fetch privileges by config item id', async () => {
        const privileges = await dbClient.fetchPrivilegesByItemId(menuId);
        sinon_1.default.assert.match(privileges.length > 0, true);
        sinon_1.default.assert.match(privileges[0].itemId, menuId);
    });
});
describe('Fetch Services for ConfigItemRelations', async () => {
    test('fetch relations by config item id', async () => {
        const relations = await dbClient.fetchAllRelationsByConfigId(menuId);
        sinon_1.default.assert.match(relations.length, 1);
        sinon_1.default.assert.match((relations[0].childItemId === menuId
            || relations[0].parentItemId === menuId), true);
    });
    test('fetch configitem by childItem id', async () => {
        const relations = await dbClient.fetchConfigItemByChildId(menuId);
        sinon_1.default.assert.match(relations.length, 1);
        sinon_1.default.assert.match(relations[0].configObjectId, parentMenuId);
    });
    test('fetch configitem by parentItem id', async () => {
        const relations = await dbClient.fetchConfigItemByParentId(parentMenuId);
        sinon_1.default.assert.match(relations.length > 0, true);
        sinon_1.default.assert.match(relations[0].configObjectType, 'Menu');
    });
    test('fetch configitem by parentItem id and relationtype', async () => {
        const relationType = 'Menu_Menu';
        const relations = await dbClient.fetchConfigItemByParentIdAndRelationType(parentMenuId, relationType);
        sinon_1.default.assert.match(relations.length > 0, true);
        sinon_1.default.assert.match(relations[0].configObjectType, 'Menu');
    });
    test('fetch configitem by parentItem id and relationtype', async () => {
        const incomeSummaryNodeId = '4b65854f-f0ef-4104-b57f-947e238fcecb';
        const relationType = 'ParentNode_ChildNode';
        const displayType = 'ADDTO_PARENT_DISPLAY';
        const relations = await dbClient.fetchChildNodesAsPerDisplayType(incomeSummaryNodeId, relationType, displayType, '1');
        sinon_1.default.assert.match(relations[0].configObjectType, 'CompositeEntityNode');
    });
});
//# sourceMappingURL=rdbms.services.using.object.test.js.map