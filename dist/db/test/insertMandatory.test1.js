"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const knex_config_1 = require("knex-config");
const uuid_1 = require("uuid");
const info_commons_1 = require("info-commons");
const constants_1 = require("../../constants");
const knex = knex_config_1.knexClient(info_commons_1.config.get(constants_1.DB), "METADB", 'sqlite3');
const query = "SELECT CHILDITEMID FROM CONFIGITEMRELATION where parentitemid in(" +
    " SELECT itemid FROM CONFIGITEMPROPERTY where propertyname='IS_MANDATORY' and"
    + " propertyvalue='1' and  itemid in(select itemid from CONFIGITEM where projectid=0))"
    + " and RELATIONTYPE='FormField_EntityColumn'";
test('insersion of mandatory properties in logical column', async () => {
    const fetchedRecords = await knex.raw(query);
    const childItemids = fetchedRecords[0];
    const properties = [];
    for (const itemid of childItemids) {
        const props = {};
        props["PROPERTYID"] = uuid_1.v4();
        props["PROPERTYNAME"] = "IS_MANDATORY";
        props["PROPERTYVALUE"] = 1;
        props["ITEMID"] = itemid.CHILDITEMID;
        props["ISDELETED"] = 0;
        props["CREATEDBY"] = "jest";
        props["CREATIONDATE"] = new Date();
        props["UPDATEDBY"] = null;
        props["UPDATIONDATE"] = null;
        props["DELETIONDATE"] = null;
        properties.push(props);
    }
    console.log("fetchedRecords", properties);
    await knex('CONFIGITEMPROPERTY').insert(properties);
});
//# sourceMappingURL=insertMandatory.test1.js.map