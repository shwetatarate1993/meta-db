"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const knex_config_1 = require("knex-config");
const constants_1 = require("../../constants");
const info_commons_1 = require("info-commons");
const mockdata_1 = require("../../mockdata");
const metadata_util_1 = require("../../utilities/metadata.util");
const knex = knex_config_1.knexClient(info_commons_1.config.get(constants_1.DB), constants_1.MOCK, 'sqlite3');
const dbClient = metadata_util_1.getAppConfigDBClient(constants_1.MOCK);
const deleteConfigPropertyItemTable = async () => {
    // Inserts seed entries
    return await knex('CONFIGITEMPROPERTY').del();
};
const deleteConfigRelationItemTable = async () => {
    // Inserts seed entries
    return await knex('CONFIGITEMRELATION').del();
};
const deleteConfigItemTable = async () => {
    // Inserts seed entries
    return await knex('CONFIGITEM').del();
};
const deleteConfigItemPrivileges = async () => {
    // Inserts seed entries
    return await knex('CONFIGITEMPRIVILEGE').del();
};
const createConfigTable = async () => {
    return knex.schema.createTableIfNotExists('CONFIGITEM', (table) => {
        table.string('ITEMID');
        table.string('ITEMNAME');
        table.string('ITEMTYPE');
        table.string('ITEMDESCRIPTION');
        table.string('PROJECTID');
        table.string('CREATEDBY');
        table.string('UPDATEDBY');
        table.timestamp('CREATIONDATE');
        table.timestamp('UPDATIONDATE');
        table.integer('ISDELETED');
        table.timestamp('DELETIONDATE');
    });
};
const createConfigPropTable = async () => {
    return knex.schema.createTableIfNotExists('CONFIGITEMPROPERTY', (table) => {
        table.string('ITEMID');
        table.string('PROPERTYID');
        table.string('PROPERTYNAME');
        table.string('PROPERTYVALUE');
        table.string('CREATEDBY');
        table.string('UPDATEDBY');
        table.timestamp('CREATIONDATE');
        table.timestamp('UPDATIONDATE');
        table.integer('ISDELETED');
        table.timestamp('DELETIONDATE');
    });
};
const createConfigRelTable = async () => {
    return knex.schema.createTableIfNotExists('CONFIGITEMRELATION', (table) => {
        table.string('RELATIONID');
        table.string('RELATIONTYPE');
        table.string('PARENTITEMID');
        table.string('CHILDITEMID');
        table.string('GROUPITEMID');
        table.string('CREATEDBY');
        table.string('UPDATEDBY');
        table.timestamp('CREATIONDATE');
        table.timestamp('UPDATIONDATE');
        table.integer('ISDELETED');
        table.timestamp('DELETIONDATE');
    });
};
const createPrivilegeTable = async () => {
    return knex.schema.createTableIfNotExists('CONFIGITEMPRIVILEGE', (table) => {
        table.string('PRIVILEGEID');
        table.integer('ROLEID');
        table.string('ITEMID');
        table.string('PRIVILEGETYPE');
        table.string('CREATEDBY');
        table.string('UPDATEDBY');
        table.timestamp('CREATIONDATE');
        table.timestamp('UPDATIONDATE');
        table.integer('ISDELETED');
        table.timestamp('DELETIONDATE');
    });
};
const tableSetup = async () => {
    const tableCreated = await createConfigTable();
    const propTableCreated = await createConfigPropTable();
    const relTableCreated = await createConfigRelTable();
    const privilegeTableCreated = await createPrivilegeTable();
    const tableDeletedProp = await deleteConfigPropertyItemTable();
    const tableDeletedRel = await deleteConfigRelationItemTable();
    const tableDeleted = await deleteConfigItemTable();
    const privilegesDeleted = await deleteConfigItemPrivileges();
};
beforeAll(async () => {
    await tableSetup();
});
afterAll(async () => {
    knex.destroy();
});
test('operations on CONFIGITEM', async () => {
    try {
        const result = await dbClient.upsertConfigItem(mockdata_1.configitemMockData[0]);
        const fetchConfig = await dbClient.fetchConfigItemByIdKnex(mockdata_1.configitemMockData[0].configObjectId);
        expect(fetchConfig.configObjectId === mockdata_1.configitemMockData[0].configObjectId).toBe(true);
        expect(fetchConfig.configObjectType).toEqual('Card');
        expect(fetchConfig.name === mockdata_1.configitemMockData[0].name).toBe(true);
    }
    catch (error) {
        throw error;
    }
});
test('operations on CONFIGITEMPROPERTY', async () => {
    try {
        await dbClient.syncConfigProperty(mockdata_1.configitemPropertyMockData, null);
        const fetchConfigProps = await dbClient.fetchConfigPropsByConfigIdKnex(mockdata_1.configitemMockData[0].configObjectId);
        expect(mockdata_1.configitemPropertyMockData.length === fetchConfigProps.length).toBe(true);
        expect(mockdata_1.configitemMockData[0].configObjectId === fetchConfigProps[0].itemId).toBe(true);
    }
    catch (error) {
        throw error;
    }
});
test('operations on CONFIGITEMRELATION', async () => {
    try {
        await dbClient.syncConfigRelation(mockdata_1.configitemRelationMockData, mockdata_1.configitemMockData[0].configObjectId);
        const relations = await dbClient.fetchAllRelationsByConfigIdKnex(mockdata_1.configitemRelationMockData[0].childItemId);
        expect(mockdata_1.configitemRelationMockData.length === relations.length).toBe(true);
        expect(relations[0].relationType).toEqual('CardGroup_Card');
    }
    catch (error) {
        throw error;
    }
});
test('operations on CONFIGITEMPRIVILEGE', async () => {
    try {
        await dbClient.syncConfigPrivilege(mockdata_1.privilegeMockData);
        const fetchConfigPrivs = await dbClient.fetchPrivilegesByItemIdKnex(mockdata_1.configitemMockData[0].configObjectId);
        const PrivsForRoleIdItemId = await dbClient.fetchPrivilegeByRoleItemId(mockdata_1.privilegeMockData[0].roleId, mockdata_1.privilegeMockData[0].itemId);
        expect(mockdata_1.privilegeMockData.length === fetchConfigPrivs.length).toBe(true);
        expect(mockdata_1.configitemMockData[0].configObjectId === fetchConfigPrivs[0].itemId).toBe(true);
        expect(PrivsForRoleIdItemId.roleId === mockdata_1.privilegeMockData[0].roleId).toBe(true);
        expect(PrivsForRoleIdItemId.itemId === mockdata_1.privilegeMockData[0].itemId).toBe(true);
    }
    catch (error) {
        throw error;
    }
});
//# sourceMappingURL=query.rdbms.execute.service.test.js.map