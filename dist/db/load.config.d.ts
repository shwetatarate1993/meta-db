import ConfigData from '../models/config.data.model';
import ConfigItemPrivilege from '../models/configitem.privilege.model';
import ConfigItemProperty from '../models/configitem.property.model';
import ConfigItemRelation from '../models/configitem.relation.model';
export declare const loadConfigData: (databaseId: string) => Promise<ConfigData>;
export declare const processProperties: (configItemProps: ConfigItemProperty[]) => Map<string, ConfigItemProperty[]>;
export declare const processPrivileges: (configItemPriv: ConfigItemPrivilege[]) => Map<string, ConfigItemPrivilege[]>;
export declare const processRelations: (configItemRels: ConfigItemRelation[]) => Map<string, ConfigItemRelation[]>;
//# sourceMappingURL=load.config.d.ts.map