import Knex from 'knex';
import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation } from '../../../models';
import { AppDatasource } from '../../datasource';
import { AppConfigDBClient } from '../dialect.interface';
export default class RDBMSAppConfigDBClient implements AppConfigDBClient {
    dialect: string;
    databaseId: string;
    knex: Knex;
    constructor(dialect: string, databaseId: string, appDatasource: AppDatasource);
    upsertConfigItem(item: ConfigItemModel): Promise<number>;
    fetchConfigItemById(id: string): Promise<ConfigItemModel>;
    fetchConfigItemByType(type: string): Promise<ConfigItemModel[]>;
    fetchConfigItemByIdKnex(id: string): Promise<ConfigItemModel>;
    fetchConfigItems(filter: object): Promise<ConfigItemModel[]>;
    fetchConfigItemsForIdList(idList: string[]): Promise<ConfigItemModel[]>;
    insertBulkConfigItem(items: ConfigItemModel[]): Promise<number>;
    fetchConfigPropsByConfigId(itemId: string): Promise<ConfigItemProperty[]>;
    fetchConfigPropsByConfigIdKnex(itemId: string): Promise<ConfigItemProperty[]>;
    syncConfigProperty(newProps: ConfigItemProperty[], options: any): Promise<void>;
    insertBulkConfigProperties(properties: ConfigItemProperty[]): Promise<number>;
    fetchConfigItemByParentIdAndRelationType(parentId: string, relationType: string): Promise<ConfigItemModel[]>;
    fetchChildNodesAsPerDisplayType(parentId: string, relationType: string, displayType: string, isCombined: string): Promise<ConfigItemModel[]>;
    fetchConfigItemByParentId(parentId: string): Promise<ConfigItemModel[]>;
    fetchConfigItemByChildId(childId: string): Promise<ConfigItemModel[]>;
    fetchRelationById(id: string): Promise<ConfigItemRelation>;
    fetchRelationByParentChildId(parentId: string, childId: string): Promise<ConfigItemRelation>;
    fetchAllRelationsByConfigId(id: string): Promise<ConfigItemRelation[]>;
    fetchAllRelationsByConfigIdKnex(id: string): Promise<ConfigItemRelation[]>;
    syncConfigRelation(relations: ConfigItemRelation[], itemId: string): Promise<void>;
    insertBulkRelations(relations: ConfigItemRelation[]): Promise<number>;
    fetchPrivilegesByItemId(id: string): Promise<ConfigItemPrivilege[]>;
    fetchPrivilegesByItemIdKnex(id: string): Promise<ConfigItemPrivilege[]>;
    syncConfigPrivilege(newPrivs: ConfigItemPrivilege[]): Promise<void>;
    fetchPrivilegeByRoleItemId(roleId: number, itemId: string): Promise<ConfigItemPrivilege>;
}
//# sourceMappingURL=appconfigdb.client.d.ts.map