"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const appconfigdb_1 = require("../../appconfigdb");
class RDBMSAppConfigDBClient {
    constructor(dialect, databaseId, appDatasource) {
        this.databaseId = databaseId;
        this.dialect = dialect;
        if (appDatasource.dsType !== 'knex') {
            throw new Error(' unsupported datasource type  :' + appDatasource.dsType);
        }
        const ds = appDatasource;
        this.knex = ds.knex;
    }
    upsertConfigItem(item) {
        return appconfigdb_1.rdbmsConfigItem.upsertConfigItem(item, this.knex);
    }
    fetchConfigItemById(id) {
        return appconfigdb_1.rdbmsConfigItem.fetchConfigItemById(id, this.knex);
    }
    fetchConfigItemByType(type) {
        return appconfigdb_1.rdbmsConfigItem.fetchConfigItemByType(type, this.knex);
    }
    fetchConfigItemByIdKnex(id) {
        return appconfigdb_1.rdbmsConfigItem.fetchConfigItemByIdKnex(id, this.knex);
    }
    fetchConfigItems(filter) {
        return appconfigdb_1.rdbmsConfigItem.fetchConfigItems(filter, this.knex);
    }
    fetchConfigItemsForIdList(idList) {
        return appconfigdb_1.rdbmsConfigItem.fetchConfigItemsForIdList(idList, this.knex);
    }
    insertBulkConfigItem(items) {
        return appconfigdb_1.rdbmsConfigItem.insertBulkConfigItem(items, this.knex);
    }
    fetchConfigPropsByConfigId(itemId) {
        return appconfigdb_1.rdbmsConfigProp.fetchConfigPropsByConfigId(itemId, this.knex);
    }
    fetchConfigPropsByConfigIdKnex(itemId) {
        return appconfigdb_1.rdbmsConfigProp.fetchConfigPropsByConfigIdknex(itemId, this.knex);
    }
    syncConfigProperty(newProps, options) {
        return appconfigdb_1.rdbmsConfigProp.syncConfigProperty(newProps, options, this.knex);
    }
    insertBulkConfigProperties(properties) {
        return appconfigdb_1.rdbmsConfigProp.insertBulkConfigProperties(properties, this.knex);
    }
    fetchConfigItemByParentIdAndRelationType(parentId, relationType) {
        return appconfigdb_1.rdbmsConfigRel.fetchConfigItemByParentIdAndRelationType(parentId, relationType, this.knex);
    }
    fetchChildNodesAsPerDisplayType(parentId, relationType, displayType, isCombined) {
        return appconfigdb_1.rdbmsConfigRel.fetchChildNodesAsPerDisplayType(parentId, relationType, displayType, isCombined, this.knex);
    }
    fetchConfigItemByParentId(parentId) {
        return appconfigdb_1.rdbmsConfigRel.fetchConfigItemByParentId(parentId, this.knex);
    }
    fetchConfigItemByChildId(childId) {
        return appconfigdb_1.rdbmsConfigRel.fetchConfigItemByChildId(childId, this.knex);
    }
    fetchRelationById(id) {
        return appconfigdb_1.rdbmsConfigRel.fetchRelationById(id, this.knex);
    }
    fetchRelationByParentChildId(parentId, childId) {
        return appconfigdb_1.rdbmsConfigRel.fetchRelationByParentChildId(parentId, childId, this.knex);
    }
    fetchAllRelationsByConfigId(id) {
        return appconfigdb_1.rdbmsConfigRel.fetchAllRelationsByConfigId(id, this.knex);
    }
    fetchAllRelationsByConfigIdKnex(id) {
        return appconfigdb_1.rdbmsConfigRel.fetchAllRelationsByConfigIdKnex(id, this.knex);
    }
    syncConfigRelation(relations, itemId) {
        return appconfigdb_1.rdbmsConfigRel.syncConfigRelation(relations, itemId, this.knex);
    }
    insertBulkRelations(relations) {
        return appconfigdb_1.rdbmsConfigRel.insertBulkRelations(relations, this.knex);
    }
    fetchPrivilegesByItemId(id) {
        return appconfigdb_1.rdbmsConfigPriv.fetchPrivilegesByItemId(id, this.knex);
    }
    fetchPrivilegesByItemIdKnex(id) {
        return appconfigdb_1.rdbmsConfigPriv.fetchPrivilegesByItemIdKnex(id, this.knex);
    }
    syncConfigPrivilege(newPrivs) {
        return appconfigdb_1.rdbmsConfigPriv.syncConfigPrivilege(newPrivs, this.knex);
    }
    fetchPrivilegeByRoleItemId(roleId, itemId) {
        return appconfigdb_1.rdbmsConfigPriv.fetchPrivilegeByRoleItemId(roleId, itemId, this.knex);
    }
}
exports.default = RDBMSAppConfigDBClient;
//# sourceMappingURL=appconfigdb.client.js.map