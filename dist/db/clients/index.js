"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../constants");
const appconfigdb_client_1 = __importDefault(require("./rdbms/appconfigdb.client"));
exports.createAppConfigDBClient = (dbEngine, databaseId, appDatasource) => {
    switch (dbEngine) {
        case constants_1.DBEngine.MYSQL:
            return new appconfigdb_client_1.default(dbEngine, databaseId, appDatasource);
        case constants_1.DBEngine.SQLLITE:
            return new appconfigdb_client_1.default(dbEngine, databaseId, appDatasource);
        default:
            throw new Error(' No Query Executor found for DBEngine ' + dbEngine);
    }
};
//# sourceMappingURL=index.js.map