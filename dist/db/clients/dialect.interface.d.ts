import { ConfigItemModel, ConfigItemPrivilege, ConfigItemProperty, ConfigItemRelation } from '../../models';
export interface AppConfigDBClient {
    dialect: string;
    databaseId: string;
    upsertConfigItem(item: ConfigItemModel): Promise<number>;
    fetchConfigItemById(id: string): Promise<ConfigItemModel>;
    fetchConfigItemByType(type: string): Promise<ConfigItemModel[]>;
    fetchConfigItemByIdKnex(id: string): Promise<ConfigItemModel>;
    fetchConfigItems(filter: object): Promise<ConfigItemModel[]>;
    fetchConfigItemsForIdList(idList: string[]): Promise<ConfigItemModel[]>;
    insertBulkConfigItem(items: ConfigItemModel[]): Promise<number>;
    fetchConfigPropsByConfigId(itemId: string): Promise<ConfigItemProperty[]>;
    fetchConfigPropsByConfigIdKnex(itemId: string): Promise<ConfigItemProperty[]>;
    syncConfigProperty(newProps: ConfigItemProperty[], options: any): Promise<void>;
    insertBulkConfigProperties(properties: ConfigItemProperty[]): Promise<number>;
    fetchChildNodesAsPerDisplayType(parentId: string, relationType: string, displayType: string, isCombined: string): Promise<ConfigItemModel[]>;
    fetchConfigItemByParentIdAndRelationType(parentId: string, relationType: string): Promise<ConfigItemModel[]>;
    fetchConfigItemByParentId(parentId: string): Promise<ConfigItemModel[]>;
    fetchConfigItemByChildId(childId: string): Promise<ConfigItemModel[]>;
    fetchRelationById(id: string): Promise<ConfigItemRelation>;
    fetchRelationByParentChildId(parentId: string, childId: string): Promise<ConfigItemRelation>;
    fetchAllRelationsByConfigId(id: string): Promise<ConfigItemRelation[]>;
    fetchAllRelationsByConfigIdKnex(id: string): Promise<ConfigItemRelation[]>;
    syncConfigRelation(relations: ConfigItemRelation[], itemId: string): Promise<void>;
    insertBulkRelations(relations: ConfigItemRelation[]): Promise<number>;
    fetchPrivilegesByItemId(id: string): Promise<ConfigItemPrivilege[]>;
    fetchPrivilegesByItemIdKnex(id: string): Promise<ConfigItemPrivilege[]>;
    syncConfigPrivilege(privileges: ConfigItemPrivilege[]): Promise<void>;
    fetchPrivilegeByRoleItemId(roleId: number, itemId: string): Promise<ConfigItemPrivilege>;
}
//# sourceMappingURL=dialect.interface.d.ts.map