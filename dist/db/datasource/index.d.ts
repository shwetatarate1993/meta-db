import Knex from 'knex';
export interface AppDatasource {
    dsType: string;
}
export declare class KnexDatasource implements AppDatasource {
    dsType: string;
    knex: Knex;
    constructor(knex: Knex);
}
//# sourceMappingURL=index.d.ts.map