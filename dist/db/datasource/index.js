'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class KnexDatasource {
    constructor(knex) {
        this.dsType = 'knex';
        this.knex = knex;
        Object.freeze(this);
    }
}
exports.KnexDatasource = KnexDatasource;
//# sourceMappingURL=index.js.map