"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const rdbmsItem = __importStar(require("./rdbms/config.item"));
const rdbmsPriv = __importStar(require("./rdbms/config.privilege"));
const rdbmsProp = __importStar(require("./rdbms/config.property"));
const rdbmsRel = __importStar(require("./rdbms/config.relation"));
exports.rdbmsConfigItem = rdbmsItem;
exports.rdbmsConfigProp = rdbmsProp;
exports.rdbmsConfigRel = rdbmsRel;
exports.rdbmsConfigPriv = rdbmsPriv;
//# sourceMappingURL=index.js.map