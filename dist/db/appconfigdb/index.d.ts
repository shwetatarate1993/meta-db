import * as rdbmsItem from './rdbms/config.item';
import * as rdbmsPriv from './rdbms/config.privilege';
import * as rdbmsProp from './rdbms/config.property';
import * as rdbmsRel from './rdbms/config.relation';
export declare const rdbmsConfigItem: typeof rdbmsItem;
export declare const rdbmsConfigProp: typeof rdbmsProp;
export declare const rdbmsConfigRel: typeof rdbmsRel;
export declare const rdbmsConfigPriv: typeof rdbmsPriv;
//# sourceMappingURL=index.d.ts.map