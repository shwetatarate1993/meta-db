import Knex from 'knex';
import { ConfigItemProperty } from '../../../models';
export declare const fetchConfigPropsByConfigIdknex: (itemId: string, knex: Knex) => Promise<ConfigItemProperty[]>;
export declare const syncConfigProperty: (newProps: ConfigItemProperty[], options: any, knex: Knex) => Promise<void>;
export declare const fetchConfigPropertyById: (id: string, knex: Knex) => Promise<ConfigItemProperty>;
export declare const fetchConfigProperties: (filter: object, knex: Knex) => Promise<ConfigItemProperty[]>;
export declare const insertBulkConfigProperties: (properties: ConfigItemProperty[], knex: Knex) => Promise<number>;
export declare const fetchAllPropertiesByProjectId: (projectId: number, knex: Knex) => Promise<ConfigItemProperty[]>;
export declare const fetchAllProperties: (knex: Knex) => Promise<ConfigItemProperty[]>;
export declare const fetchConfigPropsByConfigId: (itemId: string, knex: Knex) => Promise<ConfigItemProperty[]>;
//# sourceMappingURL=config.property.d.ts.map