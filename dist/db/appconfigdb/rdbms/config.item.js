"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../../constants");
const init_config_1 = require("../../../init-config");
const models_1 = require("../../../models");
const rename_keys_1 = require("../../../utilities/rename.keys");
// Insertion or updation of Config item
exports.upsertConfigItem = async (item, knex) => {
    const update = await isUpdate(item, knex);
    return update
        ? updateConfigItem(item, knex)
        : insertConfigItem(item, knex);
};
// fetching config items by its id
exports.fetchConfigItemByIdKnex = async (id, knex) => {
    return await knex
        .table('CONFIGITEM')
        .where('ISDELETED', 0)
        .andWhere(function () {
        this
            .where('ITEMID', id)
            .orWhere('ITEMNAME', id);
    })
        .first(...constants_1.configItemFieldsAlias);
};
// fetching config items by constraints specified in filter(input)
exports.fetchConfigItems = async (filter, knex) => {
    return await knex
        .table('CONFIGITEM')
        .where(filter)
        .andWhere('ISDELETED', 0)
        .select(...constants_1.configItemFieldsAlias);
};
// inserting config item
const insertConfigItem = async (item, knex) => {
    const cloneObject = cloneConfigObject(item, constants_1.DBOperation.INSERT);
    return await knex('CONFIGITEM').insert(rename_keys_1.renameObjectKeys(cloneObject, constants_1.configKeysTableColumns));
};
// updating config item by its id
const updateConfigItem = async (item, knex) => {
    const cloneObject = cloneConfigObject(item, constants_1.DBOperation.UPDATE);
    return await knex('CONFIGITEM')
        .update(rename_keys_1.renameObjectKeys(cloneObject, constants_1.configKeysTableColumns))
        .where({ ITEMID: item.configObjectId });
};
// return true if config item already exist
const isUpdate = async (item, knex) => {
    const isConfigItemExist = await isConfigItemExists(item.configObjectId, knex);
    return item.configObjectId && isConfigItemExist;
};
// return true if config item already exist
const isConfigItemExists = async (id, knex) => {
    return await knex
        .table('CONFIGITEM')
        .select('ITEMID')
        .where({ ITEMID: id })
        .then((rows) => {
        return rows && rows.length > 0;
    });
};
// cloning config object for audit fields
const cloneConfigObject = (item, dbOperation) => {
    const updatedconfig = new Map();
    switch (dbOperation) {
        case constants_1.DBOperation.INSERT:
            updatedconfig.set('updatedBy', null);
            updatedconfig.set('updationDate', null);
            updatedconfig.set('creationDate', item.creationDate != null
                ? item.creationDate
                : new Date());
            break;
        case constants_1.DBOperation.UPDATE:
            updatedconfig.set('updatedDate', item.updationDate != null
                ? item.updationDate
                : new Date());
            break;
    }
    return models_1.ConfigItemModel.clone(item, updatedconfig);
};
// for inserting bulk config items,not in use but kept it for future extension
exports.insertBulkConfigItem = async (items, knex) => {
    const insertConfigItems = [];
    for (const item of items) {
        await insertConfigItems.push(rename_keys_1.renameObjectKeys(item, constants_1.configKeysTableColumns));
    }
    return await knex('CONFIGITEM').insert(insertConfigItems);
};
exports.fetchAllConfigItems = async (knex) => {
    return await knex
        .table('CONFIGITEM')
        .where({ ISDELETED: 0 })
        .select(...constants_1.configItemFieldsAlias);
};
exports.fetchAllConfigItemsByProjectId = async (projectId, knex) => {
    return await knex
        .table('CONFIGITEM')
        .where({ ISDELETED: 0, PROJECTID: projectId })
        .select(...constants_1.configItemFieldsAlias);
};
exports.fetchConfigItemById = async (id, knex) => {
    return init_config_1.MetaDBConfig
        .INSTANCE
        .configItems
        .find((item) => item.configObjectId === id || item.name === id);
};
exports.fetchConfigItemByType = async (type, knex) => {
    return init_config_1.MetaDBConfig
        .INSTANCE
        .configItems
        .filter((item) => item.configObjectType === type);
};
exports.fetchConfigItemsForIdList = async (idList, knex) => {
    return init_config_1.MetaDBConfig
        .INSTANCE
        .configItems
        .filter((item) => idList.includes(item.configObjectId));
};
//# sourceMappingURL=config.item.js.map