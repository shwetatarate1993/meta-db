"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../../constants");
const init_config_1 = require("../../../init-config");
const models_1 = require("../../../models");
const rename_keys_1 = require("../../../utilities/rename.keys");
// Insertion or updation of privileges
exports.syncConfigPrivilege = async (newPriv, knex) => {
    try {
        if (newPriv && newPriv.length) {
            const itemId = newPriv[0].itemId;
            const existingPrivs = await exports.fetchPrivilegesByItemIdKnex(itemId, knex);
            // const trx = knex.transaction();
            try {
                const listOfPrivilege = processPrivileges(newPriv, existingPrivs);
                await knex('CONFIGITEMPRIVILEGE').insert(listOfPrivilege.get('INSERT'));
                listOfPrivilege.get('UPDATE').map(async (priv) => await knex('CONFIGITEMPRIVILEGE')
                    .update(priv)
                    .where({ ROLEID: priv.ROLEID, ITEMID: priv.ITEMID }));
                // trx.commit();
            }
            catch (e) {
                // trx.rollback();
                throw e;
            }
        }
    }
    catch (e) {
        throw e;
    }
};
// returning privileges to insert or update
const processPrivileges = (newPrivs, existingPrivs) => {
    const listOfPrivileges = new Map();
    listOfPrivileges.set('INSERT', []);
    listOfPrivileges.set('UPDATE', []);
    newPrivs.map((priv) => {
        const existPriv = existingPrivs.find((p) => p.roleId === priv.roleId && p.itemId === priv.itemId);
        if (existPriv == null) {
            const clonedPriv = clonePrivObject(priv, constants_1.DBOperation.INSERT);
            const privileges = listOfPrivileges.get('INSERT');
            privileges.push(rename_keys_1.renameObjectKeys(clonedPriv, constants_1.privilegeKeys));
            listOfPrivileges.set('INSERT', privileges);
        }
        if (existPriv != null && priv.privilegeType !== existPriv.privilegeType) {
            const clonedPriv = clonePrivObject(priv, constants_1.DBOperation.UPDATE);
            const privileges = listOfPrivileges.get('UPDATE');
            privileges.push(rename_keys_1.renameObjectKeys(clonedPriv, constants_1.privilegeKeys));
            listOfPrivileges.set('UPDATE', privileges);
        }
    });
    return listOfPrivileges;
};
// cloning privilege object for audit fields
const clonePrivObject = (newpriv, dbOperation) => {
    const updatedPrivs = new Map();
    switch (dbOperation) {
        case constants_1.DBOperation.INSERT:
            updatedPrivs.set('updatedBy', null);
            updatedPrivs.set('updationDate', null);
            updatedPrivs.set('deletionDate', null);
            updatedPrivs.set('creationDate', newpriv.creationDate != null ?
                newpriv.creationDate : new Date());
            break;
        case constants_1.DBOperation.UPDATE:
            updatedPrivs.set('deletionDate', null);
            updatedPrivs.set('updationDate', newpriv.updationDate != null ?
                newpriv.updationDate : new Date());
            break;
    }
    return models_1.ConfigItemPrivilege.clone(newpriv, updatedPrivs);
};
// fetching privileges by itemid
exports.fetchPrivilegesByItemIdKnex = async (itemId, knex) => {
    return await knex.table('CONFIGITEMPRIVILEGE as priv')
        .where({ ITEMID: itemId, ISDELETED: 0 })
        .select(...constants_1.privilegesColumnsAlias);
};
// fetching privilege by role and item id,return single record
exports.fetchPrivilegeByRoleItemId = async (roleId, itemId, knex) => {
    return await knex.table('CONFIGITEMPRIVILEGE as priv')
        .where({ ROLEID: roleId, ITEMID: itemId })
        .first(...constants_1.privilegesColumnsAlias);
};
exports.fetchAllPrivileges = async (knex) => {
    return await knex.table('CONFIGITEMPRIVILEGE as priv')
        .where({ ISDELETED: 0 })
        .select(...constants_1.privilegesColumnsAlias);
};
exports.fetchAllPrivilegesByProjectId = async (projectId, knex) => {
    return await knex.table('CONFIGITEMPRIVILEGE as priv')
        .leftJoin('CONFIGITEM as item', function () {
        this
            .on('item.ITEMID', 'priv.ITEMID');
    })
        .where({
        'PROJECTID': projectId, 'item.ISDELETED': 0,
        'priv.ISDELETED': 0,
    })
        .select(...constants_1.privilegesColumnsAlias);
};
exports.fetchPrivilegesByItemId = async (itemId, knex) => {
    return init_config_1.MetaDBConfig.INSTANCE.configItemPrivileges.get(itemId);
};
//# sourceMappingURL=config.privilege.js.map