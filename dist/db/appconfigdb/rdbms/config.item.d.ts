import Knex from 'knex';
import { ConfigItemModel } from '../../../models';
export declare const upsertConfigItem: (item: ConfigItemModel, knex: Knex) => Promise<number>;
export declare const fetchConfigItemByIdKnex: (id: string, knex: Knex) => Promise<ConfigItemModel>;
export declare const fetchConfigItems: (filter: object, knex: Knex) => Promise<ConfigItemModel[]>;
export declare const insertBulkConfigItem: (items: ConfigItemModel[], knex: Knex) => Promise<number>;
export declare const fetchAllConfigItems: (knex: Knex) => Promise<ConfigItemModel[]>;
export declare const fetchAllConfigItemsByProjectId: (projectId: number, knex: Knex) => Promise<ConfigItemModel[]>;
export declare const fetchConfigItemById: (id: string, knex: Knex) => Promise<ConfigItemModel>;
export declare const fetchConfigItemByType: (type: string, knex: Knex) => Promise<ConfigItemModel[]>;
export declare const fetchConfigItemsForIdList: (idList: string[], knex: Knex) => Promise<ConfigItemModel[]>;
//# sourceMappingURL=config.item.d.ts.map