import Knex from 'knex';
import { ConfigItemModel, ConfigItemRelation } from '../../../models';
export declare const fetchConfigItemByParentIdAndRelationTypeKnex: (parentId: string, relationType: string, knex: Knex) => Promise<ConfigItemModel[]>;
export declare const fetchChildNodesAsPerDisplayTypeKnex: (parentId: string, relationType: string, isCombined: string, knex: Knex) => Promise<ConfigItemModel[]>;
export declare const fetchConfigItemByParentIdKnex: (parentId: string, knex: Knex) => Promise<ConfigItemModel[]>;
export declare const fetchConfigItemByChildIdKnex: (childId: string, knex: Knex) => Promise<ConfigItemModel[]>;
export declare const fetchConfigItemByChildId: (childId: string, knex: Knex) => Promise<ConfigItemModel[]>;
export declare const fetchRelationById: (id: string, knex: Knex) => Promise<ConfigItemRelation>;
export declare const fetchRelationByParentChildId: (parentId: string, childId: string, knex: Knex) => Promise<ConfigItemRelation>;
export declare const syncConfigRelation: (relations: ConfigItemRelation[], itemId: string, knex: Knex) => Promise<void>;
export declare const fetchAllRelationsByConfigIdKnex: (id: string, knex: Knex) => Promise<ConfigItemRelation[]>;
export declare const insertBulkRelations: (relations: ConfigItemRelation[], knex: Knex) => Promise<number>;
export declare const fetchAllRelations: (knex: Knex) => Promise<ConfigItemRelation[]>;
export declare const fetchAllRelationsByProjectId: (projectId: number, knex: Knex) => Promise<ConfigItemRelation[]>;
export declare const fetchConfigItemByParentId: (parentId: string, knex: Knex) => Promise<ConfigItemModel[]>;
export declare const fetchChildNodesAsPerDisplayType: (parentId: string, relationType: string, displayType: string, isCombined: string, knex: Knex) => Promise<ConfigItemModel[]>;
export declare const fetchAllRelationsByConfigId: (id: string, knex: Knex) => Promise<ConfigItemRelation[]>;
export declare const fetchConfigItemByParentIdAndRelationType: (parentId: string, relationType: string, knex: Knex) => Promise<ConfigItemModel[]>;
//# sourceMappingURL=config.relation.d.ts.map