"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../../constants");
const init_config_1 = require("../../../init-config");
const models_1 = require("../../../models");
const rename_keys_1 = require("../../../utilities/rename.keys");
const config_item_1 = require("./config.item");
// fetching relation by parent item id & relation type
exports.fetchConfigItemByParentIdAndRelationTypeKnex = async (parentId, relationType, knex) => {
    const relationTypes = relationType.split(',');
    if (relationTypes.length === 1) {
        return await knex.table('CONFIGITEM as item')
            .leftJoin('CONFIGITEMRELATION as Rel', function () {
            this.on('item.ITEMID', 'Rel.CHILDITEMID');
        })
            .where({
            'Rel.PARENTITEMID': parentId, 'Rel.RELATIONTYPE': relationTypes[0],
            'item.ISDELETED': 0, 'Rel.ISDELETED': 0,
        })
            .select(...constants_1.configItemFieldsInJoin);
    }
    if (relationTypes.length === 2) {
        return await knex.table('CONFIGITEM as item')
            .leftJoin('CONFIGITEMRELATION as Rel', function () {
            this.on('item.ITEMID', 'Rel.CHILDITEMID');
        })
            .where({ 'Rel.PARENTITEMID': parentId, 'item.ISDELETED': 0, 'Rel.ISDELETED': 0 })
            .andWhere(function () {
            this.where('Rel.RELATIONTYPE', relationTypes[0])
                .orWhere('Rel.RELATIONTYPE', relationTypes[1]);
        })
            .select(...constants_1.configItemFieldsInJoin);
    }
};
// fetching Child CompositeEntityNodes as per display type
exports.fetchChildNodesAsPerDisplayTypeKnex = async (parentId, relationType, isCombined, knex) => {
    return await knex.table('CONFIGITEM as item')
        .leftJoin('CONFIGITEMRELATION as cir', function () {
        this.on('item.ITEMID', 'cir.CHILDITEMID');
    })
        .leftJoin('CONFIGITEMPROPERTY as cip', function () {
        this.on('cip.ITEMID', 'cir.CHILDITEMID');
    })
        .where({
        'cir.PARENTITEMID': parentId, 'cir.RELATIONTYPE': relationType,
        'item.ISDELETED': 0, 'cir.ISDELETED': 0, 'cip.PROPERTYNAME': 'ADDTO_PARENT_DISPLAY',
        'cip.PROPERTYVALUE': isCombined,
    })
        .select(...constants_1.configItemFieldsInJoin);
};
// fetching relation by parent item id
exports.fetchConfigItemByParentIdKnex = async (parentId, knex) => {
    return await knex.table('CONFIGITEM as item')
        .leftJoin('CONFIGITEMRELATION as Rel', function () {
        this
            .on('item.ITEMID', 'Rel.CHILDITEMID');
    })
        .where({
        'Rel.PARENTITEMID': parentId, 'item.ISDELETED': 0,
        'Rel.ISDELETED': 0,
    })
        .select(...constants_1.configItemFieldsInJoin);
};
// fetching relation by child item id
exports.fetchConfigItemByChildIdKnex = async (childId, knex) => {
    return await knex.table('CONFIGITEM as item')
        .leftJoin('CONFIGITEMRELATION as Rel', function () {
        this
            .on('item.ITEMID', 'Rel.PARENTITEMID');
    })
        .where({
        'Rel.CHILDITEMID': childId, 'item.ISDELETED': 0,
        'Rel.ISDELETED': 0,
    })
        .select(...constants_1.configItemFieldsInJoin);
};
exports.fetchConfigItemByChildId = async (childId, knex) => {
    const allRelations = init_config_1.MetaDBConfig.INSTANCE.configItemRelations.get(childId);
    const parentItemIds = allRelations ? await allRelations
        .filter((rel) => rel.childItemId === childId)
        .map((rel) => rel.parentItemId) : [];
    return config_item_1.fetchConfigItemsForIdList(parentItemIds, knex);
};
// fetching relation by its id
exports.fetchRelationById = async (id, knex) => {
    return await knex.table('CONFIGITEMRELATION as rel')
        .where({ RELATIONID: id, ISDELETED: 0 })
        .first(...constants_1.configRelationFields);
};
// fetching relation by parent and child item id
exports.fetchRelationByParentChildId = async (parentId, childId, knex) => {
    return await knex.table('CONFIGITEMRELATION as rel')
        .where({ PARENTITEMID: parentId, CHILDITEMID: childId })
        .first(...constants_1.configRelationFields);
};
// inserting or deleting relations
exports.syncConfigRelation = async (relations, itemId, knex) => {
    if (relations) {
        try {
            const allExistingRelations = await exports.fetchAllRelationsByConfigIdKnex(itemId, knex);
            const listOfRelations = processRelations(relations, allExistingRelations, itemId);
            await knex('CONFIGITEMRELATION').insert(listOfRelations.get('INSERT'));
            listOfRelations.get('DELETE').map(async (rel) => await knex('CONFIGITEMRELATION')
                .update(rel)
                .where({ RELATIONID: rel.RELATIONID }));
        }
        catch (e) {
            throw e;
        }
    }
};
// returning the list of relations to insert or delete
const processRelations = (newRels, existingRels, itemId) => {
    const listOfRelations = new Map();
    listOfRelations.set('INSERT', []);
    listOfRelations.set('DELETE', []);
    let newRelsAsChild = [];
    let newRelsAsParent = [];
    let existingRelsAsChild = [];
    let existingRelsAsParent = [];
    newRelsAsChild = newRels.filter((rel) => rel.childItemId === itemId);
    newRelsAsParent = newRels.filter((rel) => rel.parentItemId === itemId);
    existingRelsAsChild = existingRels.filter((rel) => rel.childItemId === itemId);
    existingRelsAsParent = existingRels.filter((rel) => rel.parentItemId === itemId);
    newRelsAsChild.map((rel) => {
        const existRelation = existingRelsAsChild.find((eRel) => eRel.parentItemId === rel.parentItemId);
        if (existRelation == null) {
            const cloneRelation = cloneRelObject(rel, constants_1.DBOperation.INSERT);
            const relations = listOfRelations.get('INSERT');
            relations.push(rename_keys_1.renameObjectKeys(cloneRelation, constants_1.configRelationKeys));
            listOfRelations.set('INSERT', relations);
        }
    });
    newRelsAsParent.map((rel) => {
        const existRelation = existingRelsAsParent.find((eRel) => eRel.childItemId === rel.childItemId);
        if (existRelation === undefined || existRelation === null) {
            const cloneRelation = cloneRelObject(rel, constants_1.DBOperation.INSERT);
            const relations = listOfRelations.get('INSERT');
            relations.push(rename_keys_1.renameObjectKeys(cloneRelation, constants_1.configRelationKeys));
            listOfRelations.set('INSERT', relations);
        }
    });
    existingRelsAsChild.map((rel) => {
        const existRelation = newRelsAsChild.find((eRel) => eRel.parentItemId === rel.parentItemId);
        if (existRelation == null) {
            const cloneRelation = cloneRelObject(rel, constants_1.DBOperation.DELETE);
            const relations = listOfRelations.get('DELETE');
            relations.push(rename_keys_1.renameObjectKeys(cloneRelation, constants_1.configRelationKeys));
            listOfRelations.set('DELETE', relations);
        }
    });
    existingRelsAsParent.map((rel) => {
        const existRelation = newRelsAsParent.find((eRel) => eRel.childItemId === rel.childItemId);
        if (existRelation == null) {
            const cloneRelation = cloneRelObject(rel, constants_1.DBOperation.DELETE);
            const relations = listOfRelations.get('DELETE');
            relations.push(rename_keys_1.renameObjectKeys(cloneRelation, constants_1.configRelationKeys));
            listOfRelations.set('DELETE', relations);
        }
    });
    return listOfRelations;
};
// cloning relation object for audit fields
const cloneRelObject = (newRel, dbOperation) => {
    const updatedRel = new Map();
    switch (dbOperation) {
        case constants_1.DBOperation.INSERT:
            updatedRel.set('updatedBy', null);
            updatedRel.set('updationDate', null);
            updatedRel.set('deletionDate', null);
            updatedRel.set('creationDate', newRel.creationDate != null ?
                newRel.creationDate : new Date());
            break;
        case constants_1.DBOperation.DELETE:
            updatedRel.set('isDeleted', 1);
            updatedRel.set('deletionDate', newRel.deletionDate != null ?
                newRel.deletionDate : new Date());
            break;
    }
    return models_1.ConfigItemRelation.clone(newRel, updatedRel);
};
// fetching relations by item id
exports.fetchAllRelationsByConfigIdKnex = async (id, knex) => {
    return await knex.table('CONFIGITEMRELATION as rel')
        .where(function () {
        this.where('PARENTITEMID', id).orWhere('CHILDITEMID', id);
    }).andWhere({ ISDELETED: 0 })
        .select(...constants_1.configRelationFields);
};
// not in use,kept it for future extension
exports.insertBulkRelations = async (relations, knex) => {
    const insertRelations = [];
    relations.map((relation) => {
        insertRelations.push(rename_keys_1.renameObjectKeys(relation, constants_1.configRelationKeys));
    });
    return await knex('CONFIGITEMRELATION').insert(insertRelations);
};
exports.fetchAllRelations = async (knex) => {
    return await knex.table('CONFIGITEMRELATION as rel')
        .where({ ISDELETED: 0 })
        .select(...constants_1.configRelationFields);
};
exports.fetchAllRelationsByProjectId = async (projectId, knex) => {
    return await knex.table('CONFIGITEMRELATION as rel')
        .leftJoin('CONFIGITEM as item', function () {
        this
            .on('item.ITEMID', 'rel.PARENTITEMID');
    })
        .where({
        'PROJECTID': projectId, 'item.ISDELETED': 0,
        'rel.ISDELETED': 0,
    })
        .select(...constants_1.configRelationFields);
};
exports.fetchConfigItemByParentId = async (parentId, knex) => {
    const allRelations = init_config_1.MetaDBConfig.INSTANCE.configItemRelations.get(parentId);
    const childItemIds = allRelations ? await allRelations
        .filter((rel) => rel.parentItemId === parentId)
        .map((rel) => rel.childItemId) : [];
    return config_item_1.fetchConfigItemsForIdList(childItemIds, knex);
};
exports.fetchChildNodesAsPerDisplayType = async (parentId, relationType, displayType, isCombined, knex) => {
    const allRelations = init_config_1.MetaDBConfig.INSTANCE.configItemRelations.get(parentId);
    const childItemIds = allRelations ? await allRelations
        .filter((rel) => rel.parentItemId === parentId && rel.relationType === relationType)
        .map((rel) => rel.childItemId) : [];
    const configItemIds = [];
    for (const childId of childItemIds) {
        const props = init_config_1.MetaDBConfig.INSTANCE.configItemProperties.get(childId);
        const prop = props.find((p) => p.propertyName === displayType && p.propertyValue === isCombined);
        if (prop) {
            configItemIds.push(prop.itemId);
        }
    }
    return config_item_1.fetchConfigItemsForIdList(configItemIds, knex);
};
exports.fetchAllRelationsByConfigId = async (id, knex) => {
    return await init_config_1.MetaDBConfig.INSTANCE.configItemRelations.get(id);
};
exports.fetchConfigItemByParentIdAndRelationType = async (parentId, relationType, knex) => {
    const relationTypes = relationType.split(',');
    const allRelations = init_config_1.MetaDBConfig.INSTANCE.configItemRelations.get(parentId);
    const childItemIds = allRelations ? await allRelations
        .filter((rel) => rel.parentItemId === parentId && relationTypes.includes(rel.relationType))
        .map((rel) => rel.childItemId) : [];
    return config_item_1.fetchConfigItemsForIdList(childItemIds, knex);
};
//# sourceMappingURL=config.relation.js.map