"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../../constants");
const init_config_1 = require("../../../init-config");
const models_1 = require("../../../models");
const rename_keys_1 = require("../../../utilities/rename.keys");
// fetching properties by item id
exports.fetchConfigPropsByConfigIdknex = async (itemId, knex) => {
    return await knex.table('CONFIGITEMPROPERTY as props')
        .select(...constants_1.configItemPropFieldsAlias)
        .where({ ITEMID: itemId, ISDELETED: 0 });
};
// Insertion or updation or deletion of properties
exports.syncConfigProperty = async (newProps, options = {}, knex) => {
    if (newProps && newProps.length) {
        const itemId = newProps[0].itemId;
        const existingProps = await exports.fetchConfigPropsByConfigIdknex(itemId, knex);
        // const trx = knex.transaction();
        try {
            const listOfProperties = processProperties(newProps, existingProps);
            await knex('CONFIGITEMPROPERTY').insert(listOfProperties.get('INSERT'));
            listOfProperties.get('UPDATE').map(async (prop) => await knex('CONFIGITEMPROPERTY')
                .update(prop)
                .where({ PROPERTYID: prop.PROPERTYID }));
            const deleteQueries = evaluateDeleteProps(newProps, existingProps, options)
                .map(async (prop) => await knex('CONFIGITEMPROPERTY')
                .update(prop)
                .where({ PROPERTYID: prop.PROPERTYID }));
            // trx.commit();
        }
        catch (e) {
            // trx.rollback();
            throw e;
        }
    }
};
// returning properties for insersion  or updation
const processProperties = (newProps, existingProps) => {
    const listOfProperties = new Map();
    listOfProperties.set('INSERT', []);
    listOfProperties.set('UPDATE', []);
    newProps.map((prop) => {
        const existProp = existingProps.find((p) => p.propertyName === prop.propertyName);
        if (existProp === undefined || existProp === null) {
            const clonedProp = clonePropsObject(prop, null, constants_1.DBOperation.INSERT);
            const properties = listOfProperties.get('INSERT');
            properties.push(rename_keys_1.renameObjectKeys(clonedProp, constants_1.configItemPropKeys));
            listOfProperties.set('INSERT', properties);
        }
        if (existProp !== undefined && existProp !== null && prop.propertyValue !== existProp.propertyValue) {
            const clonedProp = clonePropsObject(prop, existProp, constants_1.DBOperation.UPDATE);
            const properties = listOfProperties.get('UPDATE');
            properties.push(rename_keys_1.renameObjectKeys(clonedProp, constants_1.configItemPropKeys));
            listOfProperties.set('UPDATE', properties);
        }
    });
    return listOfProperties;
};
// returning properties need to delete
const evaluateDeleteProps = (newProps, existingProps, options) => {
    if (options !== undefined && options !== null && Object.keys(options).length > 0
        && (!options.isDeltaUpdate)) {
        return existingProps
            .filter((prop) => {
            const newProp = newProps.find((p) => p.propertyName === prop.propertyName);
            return newProp === undefined || newProp === null;
        })
            .map((prop) => {
            const clonedProp = clonePropsObject(null, prop, constants_1.DBOperation.DELETE);
            return rename_keys_1.renameObjectKeys(clonedProp, constants_1.configItemPropKeys);
        });
    }
    else {
        return [];
    }
};
// cloning ConfigItemProperty object
const clonePropsObject = (newProps, existingProps, dbOperation) => {
    const updatedProps = new Map();
    switch (dbOperation) {
        case constants_1.DBOperation.INSERT:
            updatedProps.set('updatedBy', null);
            updatedProps.set('updationDate', null);
            updatedProps.set('creationDate', newProps.creationDate != null ?
                newProps.creationDate : new Date());
            break;
        case constants_1.DBOperation.UPDATE:
            updatedProps.set('propertyId', existingProps.propertyId);
            updatedProps.set('updationDate', newProps.updationDate != null ?
                newProps.updationDate : new Date());
            break;
        case constants_1.DBOperation.DELETE:
            updatedProps.set('propertyId', existingProps.propertyId);
            updatedProps.set('isDeleted', 1);
            updatedProps.set('deletionDate', new Date());
            break;
    }
    return models_1.ConfigItemProperty.clone(newProps, updatedProps);
};
// fetching property by its id
exports.fetchConfigPropertyById = async (id, knex) => {
    return await knex.table('CONFIGITEMPROPERTY as props')
        .where({ PROPERTYID: id })
        .first(...constants_1.configItemPropFieldsAlias);
};
// fetching property by constraints specified in filter
exports.fetchConfigProperties = async (filter, knex) => {
    return await knex.table('CONFIGITEMPROPERTY as props')
        .where(filter)
        .select(...constants_1.configItemPropFieldsAlias);
};
// for inserting bulk properties
exports.insertBulkConfigProperties = async (properties, knex) => {
    const insertProperties = [];
    properties.map((property) => {
        insertProperties.push(rename_keys_1.renameObjectKeys(property, constants_1.configItemPropKeys));
    });
    return await knex('CONFIGITEMPROPERTY').insert(insertProperties);
};
exports.fetchAllPropertiesByProjectId = async (projectId, knex) => {
    return await knex.table('CONFIGITEMPROPERTY as props')
        .leftJoin('CONFIGITEM as item', function () {
        this
            .on('item.ITEMID', 'props.ITEMID');
    })
        .where({
        'PROJECTID': projectId, 'item.ISDELETED': 0,
        'props.ISDELETED': 0
    })
        .whereNotNull('props.propertyValue')
        .select(...constants_1.configItemPropFieldsAlias);
};
exports.fetchAllProperties = async (knex) => {
    return await knex.table('CONFIGITEMPROPERTY as props')
        .select(...constants_1.configItemPropFieldsAlias)
        .where({ ISDELETED: 0 });
};
exports.fetchConfigPropsByConfigId = async (itemId, knex) => {
    return init_config_1.MetaDBConfig.INSTANCE.configItemProperties.get(itemId);
};
//# sourceMappingURL=config.property.js.map