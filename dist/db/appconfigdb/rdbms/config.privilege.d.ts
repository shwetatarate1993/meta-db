import Knex from 'knex';
import { ConfigItemPrivilege } from '../../../models';
export declare const syncConfigPrivilege: (newPriv: ConfigItemPrivilege[], knex: Knex) => Promise<void>;
export declare const fetchPrivilegesByItemIdKnex: (itemId: string, knex: Knex) => Promise<ConfigItemPrivilege[]>;
export declare const fetchPrivilegeByRoleItemId: (roleId: number, itemId: string, knex: Knex) => Promise<ConfigItemPrivilege>;
export declare const fetchAllPrivileges: (knex: Knex) => Promise<ConfigItemPrivilege[]>;
export declare const fetchAllPrivilegesByProjectId: (projectId: number, knex: Knex) => Promise<ConfigItemPrivilege[]>;
export declare const fetchPrivilegesByItemId: (itemId: string, knex: Knex) => Promise<ConfigItemPrivilege[]>;
//# sourceMappingURL=config.privilege.d.ts.map