"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_data_model_1 = __importDefault(require("../models/config.data.model"));
const jsonconfig_1 = require("./jsonconfig");
const appconfigdb_1 = require("./appconfigdb");
const info_commons_1 = require("info-commons");
let knexClient;
if (info_commons_1.APP_ENV === 'server') {
    knexClient = require("knex-config").knexClient;
}
const constants_1 = require("../constants");
exports.loadConfigData = async (databaseId) => {
    console.log('loadConfigData');
    let start = new Date();
    let end = new Date();
    let configItem;
    let configItemProps;
    let configItemRels;
    let configItemPriv;
    if (info_commons_1.APP_ENV === "desktop") {
        configItem = jsonconfig_1.configitemsjson;
        configItemProps = jsonconfig_1.configitempropertyjson;
        configItemRels = jsonconfig_1.configitemrelationjson;
        configItemPriv = jsonconfig_1.configitemprivjson;
    }
    else {
        const knex = knexClient(info_commons_1.config.get(constants_1.DB), databaseId, info_commons_1.DIALECT);
        const projectId = 0;
        start = new Date();
        configItem = await appconfigdb_1.rdbmsConfigItem.fetchAllConfigItemsByProjectId(projectId, knex);
        end = new Date();
        console.info('Execution time: %dms', end.getTime() - start.getTime());
        start = new Date();
        configItemProps = await appconfigdb_1.rdbmsConfigProp.fetchAllPropertiesByProjectId(projectId, knex);
        end = new Date();
        console.info('Execution time: %dms', end.getTime() - start.getTime());
        start = new Date();
        configItemRels = await appconfigdb_1.rdbmsConfigRel.fetchAllRelationsByProjectId(projectId, knex);
        end = new Date();
        console.info('Execution time: %dms', end.getTime() - start.getTime());
        start = new Date();
        configItemPriv = await appconfigdb_1.rdbmsConfigPriv.fetchAllPrivilegesByProjectId(projectId, knex);
        end = new Date();
        console.info('Execution time: %dms', end.getTime() - start.getTime());
    }
    start = new Date();
    const propsMap = exports.processProperties(configItemProps);
    const privMap = exports.processPrivileges(configItemPriv);
    const relMap = exports.processRelations(configItemRels);
    end = new Date();
    console.info('Execution time: %dms', end.getTime() - start.getTime());
    console.info('configItem ', configItem.length);
    console.info('configItemProps ', configItemProps.length);
    console.info('configItemPriv ', configItemPriv.length);
    console.info('configItemRels ', configItemRels.length);
    return new config_data_model_1.default(configItem, propsMap, privMap, relMap);
};
exports.processProperties = (configItemProps) => {
    const propsMap = new Map();
    for (const propItem of configItemProps) {
        if (propsMap.get(propItem.itemId)) {
            const properties = propsMap.get(propItem.itemId);
            properties.push(propItem);
            propsMap.set(propItem.itemId, properties);
        }
        else {
            const properties = [];
            properties.push(propItem);
            propsMap.set(propItem.itemId, properties);
        }
    }
    return propsMap;
};
exports.processPrivileges = (configItemPriv) => {
    const privMap = new Map();
    for (const priv of configItemPriv) {
        if (privMap.get(priv.itemId)) {
            const privileges = privMap.get(priv.itemId);
            privileges.push(priv);
            privMap.set(priv.itemId, privileges);
        }
        else {
            const privileges = [];
            privileges.push(priv);
            privMap.set(priv.itemId, privileges);
        }
    }
    return privMap;
};
exports.processRelations = (configItemRels) => {
    const relMap = new Map();
    for (const rel of configItemRels) {
        if (relMap.get(rel.parentItemId)) {
            const parentRels = relMap.get(rel.parentItemId);
            parentRels.push(rel);
            relMap.set(rel.parentItemId, parentRels);
        }
        else {
            const parentRels = [];
            parentRels.push(rel);
            relMap.set(rel.parentItemId, parentRels);
        }
        if (relMap.get(rel.childItemId)) {
            const childRels = relMap.get(rel.childItemId);
            childRels.push(rel);
            relMap.set(rel.childItemId, childRels);
        }
        else {
            const childRels = [];
            childRels.push(rel);
            relMap.set(rel.childItemId, childRels);
        }
    }
    return relMap;
};
//# sourceMappingURL=load.config.js.map