"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.configKeysTableColumns = {
    configObjectId: 'ITEMID', name: 'ITEMNAME', configObjectType: 'ITEMTYPE',
    createdBy: 'CREATEDBY', isDeleted: 'ISDELETED',
    itemDescription: 'ITEMDESCRIPTION', projectId: 'PROJECTID',
    updatedBy: 'UPDATEDBY', creationDate: 'CREATIONDATE',
    updationDate: 'UPDATIONDATE', deletionDate: 'DELETIONDATE',
    graphData: 'GRAPH_DATA', graphImage: 'GRAPH_IMAGE',
};
exports.configItemPropFieldsAlias = ['PROPERTYNAME as propertyName',
    'PROPERTYVALUE as propertyValue', 'props.ITEMID as itemId'];
exports.configItemFieldsAlias = ['ITEMID as configObjectId', 'ITEMNAME as name',
    'ITEMTYPE as configObjectType', 'CREATEDBY as createdBy', 'UPDATEDBY as updatedBy',
    'CREATIONDATE as creationDate', 'UPDATIONDATE as updationDate',
    'ITEMDESCRIPTION as itemDescription', 'ISDELETED as isDeleted',
    'DELETIONDATE as deletionDate', 'PROJECTID as projectId'];
exports.configItemPropKeys = {
    propertyId: 'PROPERTYID', propertyName: 'PROPERTYNAME',
    propertyValue: 'PROPERTYVALUE', itemId: 'ITEMID', createdBy: 'CREATEDBY',
    isDeleted: 'ISDELETED', creationDate: 'CREATIONDATE', updatedBy: 'UPDATEDBY',
    updationDate: 'UPDATIONDATE', deletionDate: 'DELETIONDATE',
};
exports.configItemFieldsInJoin = ['item.ITEMID as configObjectId', 'ITEMNAME as name',
    'ITEMTYPE as configObjectType', 'item.CREATEDBY as createdBy', 'item.UPDATEDBY as updatedBy',
    'item.CREATIONDATE as creationDate', 'item.UPDATIONDATE as updationDate',
    'ITEMDESCRIPTION as itemDescription', 'item.ISDELETED as isDeleted',
    'item.DELETIONDATE as deletionDate'];
exports.configRelationFields = ['RELATIONID as relationId',
    'RELATIONTYPE as relationType',
    'PARENTITEMID as parentItemId', 'CHILDITEMID as childItemId',
    'rel.CREATEDBY as createdBy', 'rel.UPDATEDBY as updatedBy',
    'rel.CREATIONDATE as creationDate', 'rel.UPDATIONDATE as updationDate',
    'rel.ISDELETED as isDeleted', 'rel.DELETIONDATE as deletionDate'];
exports.configRelationKeys = {
    relationId: 'RELATIONID', relationType: 'RELATIONTYPE',
    parentItemId: 'PARENTITEMID', childItemId: 'CHILDITEMID',
    createdBy: 'CREATEDBY', isDeleted: 'ISDELETED',
    updatedBy: 'UPDATEDBY', creationDate: 'CREATIONDATE',
    updationDate: 'UPDATIONDATE', deletionDate: 'DELETIONDATE',
};
exports.privilegesColumnsAlias = ['PRIVILEGEID as privilegeId', 'priv.ITEMID as itemId',
    'ROLEID as roleId', 'PRIVILEGETYPE as privilegeType', 'priv.CREATEDBY as createdBy',
    'priv.UPDATEDBY as updatedBy', 'priv.CREATIONDATE as creationDate', 'priv.UPDATIONDATE as updationDate',
    'priv.ISDELETED as isDeleted', 'priv.DELETIONDATE as deletionDate'];
exports.privilegeKeys = {
    privilegeId: 'PRIVILEGEID', roleId: 'ROLEID',
    privilegeType: 'PRIVILEGETYPE', itemId: 'ITEMID', createdBy: 'CREATEDBY',
    isDeleted: 'ISDELETED', creationDate: 'CREATIONDATE', updatedBy: 'UPDATEDBY',
    updationDate: 'UPDATIONDATE', deletionDate: 'DELETIONDATE',
};
//# sourceMappingURL=config.metadata.js.map