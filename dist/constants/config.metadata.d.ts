export declare const configKeysTableColumns: object;
export declare const configItemPropFieldsAlias: string[];
export declare const configItemFieldsAlias: string[];
export declare const configItemPropKeys: object;
export declare const configItemFieldsInJoin: string[];
export declare const configRelationFields: string[];
export declare const configRelationKeys: object;
export declare const privilegesColumnsAlias: string[];
export declare const privilegeKeys: object;
//# sourceMappingURL=config.metadata.d.ts.map