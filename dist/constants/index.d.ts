export { configItemFieldsAlias, configItemPropFieldsAlias, configKeysTableColumns, configItemPropKeys, configItemFieldsInJoin, configRelationFields, configRelationKeys, privilegesColumnsAlias, privilegeKeys, } from './config.metadata';
export { parseDBEngineEnum, DBEngine } from './dbengine.enum';
export { DBOperation } from './constant.enum';
export declare const MOCK = "mock";
export declare const DB = "db";
export declare const METADB = "METADB";
//# sourceMappingURL=index.d.ts.map