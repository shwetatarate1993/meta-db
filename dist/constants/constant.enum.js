"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DBOperation;
(function (DBOperation) {
    DBOperation["INSERT"] = "insert";
    DBOperation["UPDATE"] = "update";
    DBOperation["DELETE"] = "delete";
})(DBOperation = exports.DBOperation || (exports.DBOperation = {}));
//# sourceMappingURL=constant.enum.js.map