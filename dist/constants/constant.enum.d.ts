export declare enum DBOperation {
    INSERT = "insert",
    UPDATE = "update",
    DELETE = "delete"
}
//# sourceMappingURL=constant.enum.d.ts.map