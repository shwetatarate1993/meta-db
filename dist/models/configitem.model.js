'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class ConfigItemModel {
    constructor(itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted) {
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
    }
    static clone(existingProps, newProps) {
        return new ConfigItemModel(newProps.has('configObjectId') ? newProps.get('configObjectId') :
            existingProps.configObjectId, newProps.has('name') ? newProps.get('name') : existingProps.name, newProps.has('configObjectType') ? newProps.get('configObjectType') : existingProps.configObjectType, newProps.has('projectId') ? newProps.get('projectId') : existingProps.projectId, newProps.has('createdBy') ? newProps.get('createdBy') : existingProps.createdBy, newProps.has('itemDescription') ? newProps.get('itemDescription') : existingProps.itemDescription, newProps.has('creationDate') ? newProps.get('creationDate') : existingProps.creationDate, newProps.has('updatedBy') ? newProps.get('updatedBy') : existingProps.updatedBy, newProps.has('updationDate') ? newProps.get('updationDate') : existingProps.updationDate, newProps.has('deletionDate') ? newProps.get('deletionDate') : existingProps.deletionDate, newProps.has('isDeleted') ? newProps.get('isDeleted') : existingProps.isDeleted);
    }
}
exports.default = ConfigItemModel;
//# sourceMappingURL=configitem.model.js.map