'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class ConfigItemProperty {
    constructor(propertyName, propertyValue, itemId, propertyId, createdBy, isDeleted, creationDate, updatedBy, updationDate, deletionDate) {
        this.propertyId = propertyId;
        this.propertyName = propertyName;
        this.propertyValue = propertyValue;
        this.itemId = itemId;
        this.createdBy = createdBy;
        this.isDeleted = isDeleted;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        Object.freeze(this);
    }
    static clone(existingProps, newProps) {
        return new ConfigItemProperty(newProps.has('propertyName') ?
            newProps.get('propertyName') : existingProps.propertyName, newProps.has('propertyValue') ?
            newProps.get('propertyValue') : existingProps.propertyValue, newProps.has('itemId') ?
            newProps.get('itemId') : existingProps.itemId, newProps.has('propertyId') ?
            newProps.get('propertyId') : existingProps.propertyId, newProps.has('createdBy') ?
            newProps.get('createdBy') : existingProps.createdBy, newProps.has('isDeleted') ?
            newProps.get('isDeleted') : existingProps.isDeleted, newProps.has('creationDate') ?
            newProps.get('creationDate') : existingProps.creationDate, newProps.has('updatedBy') ?
            newProps.get('updatedBy') : existingProps.updatedBy, newProps.has('updationDate') ?
            newProps.get('updationDate') : existingProps.updationDate, newProps.has('deletionDate') ?
            newProps.get('deletionDate') : existingProps.deletionDate);
    }
}
exports.default = ConfigItemProperty;
//# sourceMappingURL=configitem.property.model.js.map