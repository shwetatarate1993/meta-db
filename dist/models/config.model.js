'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class Config {
    constructor(configItem, properties, relations, privileges) {
        this.configItem = configItem;
        this.configItemProperty = properties;
        this.configItemRelation = relations;
        this.configItemPrivilege = privileges;
        Object.freeze(this);
    }
}
exports.default = Config;
//# sourceMappingURL=config.model.js.map