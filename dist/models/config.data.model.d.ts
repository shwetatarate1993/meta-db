import ConfigItem from './configitem.model';
import ConfigItemPrivilege from './configitem.privilege.model';
import ConfigItemProperty from './configitem.property.model';
import ConfigItemRelation from './configitem.relation.model';
export default class ConfigData {
    configItems: ConfigItem[];
    configItemProperties: Map<string, ConfigItemProperty[]>;
    configItemPrivileges: Map<string, ConfigItemPrivilege[]>;
    configItemRelations: Map<string, ConfigItemRelation[]>;
    constructor(configItems: ConfigItem[], configItemProperties: Map<string, ConfigItemProperty[]>, configItemPrivileges: Map<string, ConfigItemPrivilege[]>, configItemRelations: Map<string, ConfigItemRelation[]>);
}
//# sourceMappingURL=config.data.model.d.ts.map