import ConfigItemPrivilege from './configitem.privilege.model';
import ConfigItemRelation from './configitem.relation.model';
export interface ConfigItem {
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy?: string;
    isDeleted?: number;
    itemDescription?: string;
    creationDate?: Date;
    projectId?: number;
    updatedBy?: string;
    updationDate?: Date;
    deletionDate?: Date;
    privileges?: ConfigItemPrivilege[];
    childRelations?: ConfigItemRelation[];
    parentRelations?: ConfigItemRelation[];
}
//# sourceMappingURL=configitem.interface.d.ts.map