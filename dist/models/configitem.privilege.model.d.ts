export default class ConfigItemPrivilege {
    static clone(existingPriv: ConfigItemPrivilege, newPriv: Map<string, any>): ConfigItemPrivilege;
    static deserialize(input: any): ConfigItemPrivilege;
    privilegeId?: string;
    privilegeType?: string;
    itemId: string;
    roleId: number;
    createdBy?: string;
    isDeleted?: number;
    creationDate?: Date;
    updatedBy?: string;
    updationDate?: Date;
    deletionDate?: Date;
    constructor(privilegeType: string, itemId: string, roleId: number, privilegeId?: string, createdBy?: string, isDeleted?: number, creationDate?: Date, updatedBy?: string, updationDate?: Date, deletionDate?: Date);
}
//# sourceMappingURL=configitem.privilege.model.d.ts.map