'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class ConfigItemPrivilege {
    constructor(privilegeType, itemId, roleId, privilegeId, createdBy, isDeleted, creationDate, updatedBy, updationDate, deletionDate) {
        this.privilegeId = privilegeId;
        this.privilegeType = privilegeType;
        this.itemId = itemId;
        this.roleId = roleId;
        this.createdBy = createdBy;
        this.isDeleted = isDeleted;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        Object.freeze(this);
    }
    static clone(existingPriv, newPriv) {
        return new ConfigItemPrivilege(newPriv.has('privilegeType') ?
            newPriv.get('privilegeType') : existingPriv.privilegeType, newPriv.has('itemId') ?
            newPriv.get('itemId') : existingPriv.itemId, newPriv.has('roleId') ?
            newPriv.get('roleId') : existingPriv.roleId, newPriv.has('privilegeId') ?
            newPriv.get('privilegeId') : existingPriv.privilegeId, newPriv.has('createdBy') ?
            newPriv.get('createdBy') : existingPriv.createdBy, newPriv.has('isDeleted') ?
            newPriv.get('isDeleted') : existingPriv.isDeleted, newPriv.has('creationDate') ?
            newPriv.get('creationDate') : existingPriv.creationDate, newPriv.has('updatedBy') ?
            newPriv.get('updatedBy') : existingPriv.updatedBy, newPriv.has('updationDate') ?
            newPriv.get('updationDate') : existingPriv.updationDate, newPriv.has('deletionDate') ?
            newPriv.get('deletionDate') : existingPriv.deletionDate);
    }
    static deserialize(input) {
        return new ConfigItemPrivilege(input.privilegeType !== undefined ? input.privilegeType : null, input.itemId !== undefined ? input.itemId : null, input.roleId !== undefined ? input.roleId : null, input.privilegeId !== undefined ? input.privilegeId : null, input.createdBy !== undefined ? input.createdBy : null, input.isDeleted !== undefined ? input.isDeleted : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null);
    }
}
exports.default = ConfigItemPrivilege;
//# sourceMappingURL=configitem.privilege.model.js.map