'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class ConfigItemRelation {
    constructor(relationType, parentItemId, childItemId, relationId, createdBy, isDeleted, creationDate, updatedBy, updationDate, deletionDate) {
        this.relationId = relationId;
        this.relationType = relationType;
        this.parentItemId = parentItemId;
        this.childItemId = childItemId;
        this.createdBy = createdBy;
        this.isDeleted = isDeleted;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        Object.freeze(this);
    }
    static clone(existingRel, newRel) {
        return new ConfigItemRelation(newRel.has('relationType') ? newRel.get('relationType') : existingRel.relationType, newRel.has('parentItemId') ? newRel.get('parentItemId') : existingRel.parentItemId, newRel.has('childItemId') ? newRel.get('childItemId') : existingRel.childItemId, newRel.has('relationId') ? newRel.get('relationId') : existingRel.relationId, newRel.has('createdBy') ? newRel.get('createdBy') : existingRel.createdBy, newRel.has('isDeleted') ? newRel.get('isDeleted') : existingRel.isDeleted, newRel.has('creationDate') ? newRel.get('creationDate') : existingRel.creationDate, newRel.has('updatedBy') ? newRel.get('updatedBy') : existingRel.updatedBy, newRel.has('updationDate') ? newRel.get('updationDate') : existingRel.updationDate, newRel.has('deletionDate') ? newRel.get('deletionDate') : existingRel.deletionDate);
    }
    static deserialize(input) {
        return new ConfigItemRelation(input.relationType !== undefined ? input.relationType : null, input.parentItemId !== undefined ? input.parentItemId : null, input.childItemId !== undefined ? input.childItemId : null, input.relationId !== undefined ? input.relationId : null, input.createdBy !== undefined ? input.createdBy : null, input.isDeleted !== undefined ? input.isDeleted : null, input.creationDate !== undefined ? input.creationDate : null, input.updatedBy !== undefined ? input.updatedBy : null, input.updationDate !== undefined ? input.updationDate : null, input.deletionDate !== undefined ? input.deletionDate : null);
    }
}
exports.default = ConfigItemRelation;
//# sourceMappingURL=configitem.relation.model.js.map