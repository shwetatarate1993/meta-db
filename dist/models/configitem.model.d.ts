import { ConfigItem } from './configitem.interface';
export default class ConfigItemModel implements ConfigItem {
    static clone(existingProps: ConfigItemModel, newProps: Map<string, any>): ConfigItemModel;
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy?: string;
    isDeleted?: number;
    itemDescription?: string;
    creationDate?: Date;
    projectId?: number;
    updatedBy?: string;
    updationDate?: Date;
    deletionDate?: Date;
    constructor(itemId: string, itemName: string, itemType: string, projectId?: number, createdBy?: string, itemDescription?: string, creationDate?: Date, updatedBy?: string, updationDate?: Date, deletionDate?: Date, isDeleted?: number);
}
//# sourceMappingURL=configitem.model.d.ts.map