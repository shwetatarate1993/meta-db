export { default as ConfigMetadata } from './config.model';
export { default as ConfigItemModel } from './configitem.model';
export { default as ConfigItemProperty } from './configitem.property.model';
export { default as ConfigItemRelation } from './configitem.relation.model';
export { default as ConfigItemPrivilege } from './configitem.privilege.model';
export { ConfigItem } from './configitem.interface';
//# sourceMappingURL=index.d.ts.map