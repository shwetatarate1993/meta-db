import { ConfigItem } from './configitem.interface';
export default class ConfigItemRelation {
    static clone(existingRel: ConfigItemRelation, newRel: Map<string, any>): ConfigItemRelation;
    static deserialize(input: any): ConfigItemRelation;
    relationId?: string;
    relationType: string;
    parentItemId: string;
    childItemId: string;
    createdBy?: string;
    isDeleted?: number;
    creationDate?: Date;
    updatedBy?: string;
    updationDate?: Date;
    deletionDate?: Date;
    configObject?: ConfigItem;
    constructor(relationType: string, parentItemId: string, childItemId: string, relationId?: string, createdBy?: string, isDeleted?: number, creationDate?: Date, updatedBy?: string, updationDate?: Date, deletionDate?: Date);
}
//# sourceMappingURL=configitem.relation.model.d.ts.map