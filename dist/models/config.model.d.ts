import ConfigItemModel from './configitem.model';
import ConfigItemPrivilege from './configitem.privilege.model';
import ConfigItemProperty from './configitem.property.model';
import ConfigItemRelation from './configitem.relation.model';
export default class Config {
    configItem: ConfigItemModel;
    configItemProperty: ConfigItemProperty[];
    configItemRelation: ConfigItemRelation[];
    configItemPrivilege: ConfigItemPrivilege[];
    constructor(configItem: ConfigItemModel, properties: ConfigItemProperty[], relations?: ConfigItemRelation[], privileges?: ConfigItemPrivilege[]);
}
//# sourceMappingURL=config.model.d.ts.map