export default class ConfigItemProperty {
    static clone(existingProps: ConfigItemProperty, newProps: Map<string, any>): ConfigItemProperty;
    propertyId?: string;
    propertyName: string;
    propertyValue: any;
    itemId: string;
    createdBy?: string;
    isDeleted?: number;
    creationDate?: Date;
    updatedBy?: string;
    updationDate?: Date;
    deletionDate?: Date;
    constructor(propertyName: string, propertyValue: any, itemId: string, propertyId?: string, createdBy?: string, isDeleted?: number, creationDate?: Date, updatedBy?: string, updationDate?: Date, deletionDate?: Date);
}
//# sourceMappingURL=configitem.property.model.d.ts.map