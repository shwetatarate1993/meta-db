"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ConfigData {
    constructor(configItems, configItemProperties, configItemPrivileges, configItemRelations) {
        this.configItems = configItems;
        this.configItemProperties = configItemProperties;
        this.configItemPrivileges = configItemPrivileges;
        this.configItemRelations = configItemRelations;
        Object.freeze(this);
    }
}
exports.default = ConfigData;
//# sourceMappingURL=config.data.model.js.map