"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var config_model_1 = require("./config.model");
exports.ConfigMetadata = config_model_1.default;
var configitem_model_1 = require("./configitem.model");
exports.ConfigItemModel = configitem_model_1.default;
var configitem_property_model_1 = require("./configitem.property.model");
exports.ConfigItemProperty = configitem_property_model_1.default;
var configitem_relation_model_1 = require("./configitem.relation.model");
exports.ConfigItemRelation = configitem_relation_model_1.default;
var configitem_privilege_model_1 = require("./configitem.privilege.model");
exports.ConfigItemPrivilege = configitem_privilege_model_1.default;
//# sourceMappingURL=index.js.map