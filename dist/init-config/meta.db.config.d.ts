import ConfigData from '../models/config.data.model';
export default class MetaDBConfig {
    static readonly INSTANCE: ConfigData;
    static configure(databaseId: string): Promise<void>;
    private static configDataLoader;
    private constructor();
}
//# sourceMappingURL=meta.db.config.d.ts.map