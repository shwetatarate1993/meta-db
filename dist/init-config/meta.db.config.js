"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const load_config_1 = require("../db/load.config");
const constants_1 = require("../constants");
class MetaDBConfig {
    constructor() {
        /** No Op */
    }
    static get INSTANCE() {
        (async () => await MetaDBConfig.configure(constants_1.METADB))();
        return MetaDBConfig.configDataLoader;
    }
    static async configure(databaseId) {
        if (!MetaDBConfig.configDataLoader) {
            const configData = await load_config_1.loadConfigData(databaseId);
            MetaDBConfig.configDataLoader = configData;
        }
    }
}
exports.default = MetaDBConfig;
//# sourceMappingURL=meta.db.config.js.map