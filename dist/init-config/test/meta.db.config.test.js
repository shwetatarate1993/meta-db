"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const meta_db_config_1 = __importDefault(require("../meta.db.config"));
const jsonconfig_1 = require("../../db/jsonconfig");
const load_config_1 = require("../../db/load.config");
const config_data_model_1 = __importDefault(require("../../models/config.data.model"));
const loadDataUtils = __importStar(require("../../db/load.config"));
const sinon_1 = __importDefault(require("sinon"));
const constants_1 = require("../../constants");
let loadConfigMock;
beforeAll(() => {
    const props = load_config_1.processProperties(jsonconfig_1.configitempropertyjson);
    const rels = load_config_1.processRelations(jsonconfig_1.configitemrelationjson);
    const privs = load_config_1.processPrivileges(jsonconfig_1.configitemprivjson);
    const configObject = new config_data_model_1.default(jsonconfig_1.configitemsjson, props, privs, rels);
    loadConfigMock = sinon_1.default.mock(loadDataUtils);
    loadConfigMock.expects('loadConfigData').once().returns(configObject);
});
afterAll(() => {
    loadConfigMock.restore();
});
test('configure meta data', async () => {
    meta_db_config_1.default.configure(constants_1.MOCK);
    loadConfigMock.verify();
});
test('Get instance of metadata and Verifying that loadConfigData is not called again', () => {
    const configMetaData = meta_db_config_1.default.INSTANCE;
    sinon_1.default.assert.match(configMetaData instanceof config_data_model_1.default, true);
    loadConfigMock.verify();
});
//# sourceMappingURL=meta.db.config.test.js.map