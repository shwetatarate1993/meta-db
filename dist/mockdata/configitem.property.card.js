"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    {
        propertyId: '8f801c10-1b1d-11e9-b2e1-ad14d7833e95',
        propertyName: 'CARD_TYPE',
        propertyValue: 'Grid',
        itemId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
        createdBy: '1119',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '8f801c10-1b1d-11e9-b2e1-ad14d7833e96',
        propertyName: 'DATAGRID_ID',
        propertyValue: '6014475a-f25b-4df2-a7f0-23342daaff5c',
        itemId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
        createdBy: '1119',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
    {
        propertyId: '8f8c10-1b1d-11e9-b2e1-ad14d7833e96',
        propertyName: 'RENDERING_BEAN_NAME',
        propertyValue: 'com.appengine.view.web.component.builder.DataGridViewCardBuilder',
        itemId: '1bc9d82d-0728-43c7-a228-5e0eba9071c1',
        createdBy: '1119',
        updatedBy: null,
        creationDate: null,
        isDeleted: 0,
        deletionDate: null,
        updationDate: null,
    },
];
//# sourceMappingURL=configitem.property.card.js.map