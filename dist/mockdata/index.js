"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var configitem_card_1 = require("./configitem.card");
exports.configitemMockData = configitem_card_1.default;
var configitem_privileges_card_1 = require("./configitem.privileges.card");
exports.privilegeMockData = configitem_privileges_card_1.default;
var configitem_property_card_1 = require("./configitem.property.card");
exports.configitemPropertyMockData = configitem_property_card_1.default;
var configitem_relation_card_1 = require("./configitem.relation.card");
exports.configitemRelationMockData = configitem_relation_card_1.default;
//# sourceMappingURL=index.js.map