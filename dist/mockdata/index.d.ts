export { default as configitemMockData } from './configitem.card';
export { default as privilegeMockData } from './configitem.privileges.card';
export { default as configitemPropertyMockData } from './configitem.property.card';
export { default as configitemRelationMockData } from './configitem.relation.card';
//# sourceMappingURL=index.d.ts.map