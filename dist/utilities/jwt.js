"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_jwt_1 = __importDefault(require("express-jwt"));
const info_commons_1 = require("info-commons");
module.exports = jwt;
function jwt() {
    const secret = info_commons_1.config.get('secret');
    return express_jwt_1.default({ secret }).unless({
        path: [
            // public routes that don't require authentication
            '/user/authenticate',
        ],
    });
}
//# sourceMappingURL=jwt.js.map