"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// todo :unable to use import
/* tslint:disable-next-line:no-var-requires */
const renameKeys = require('rename-keys');
// Function to rename keys in config Object as per CONFIGITEM table
exports.renameObjectKeys = (configObject, configKeys) => {
    const configKeysObj = Object(configKeys);
    const renamedConfigKeys = renameKeys(configObject, (key, val) => {
        return configKeysObj[key];
    });
    return renamedConfigKeys;
};
//# sourceMappingURL=rename.keys.js.map