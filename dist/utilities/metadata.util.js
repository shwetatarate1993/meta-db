"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const knex_config_1 = require("knex-config");
const constants_1 = require("../constants");
const clients_1 = require("../db/clients");
const datasource_1 = require("../db/datasource");
const configDBCache = new Map();
exports.getAppConfigDBClient = (databaseId) => {
    let dbClient = configDBCache.get(databaseId);
    let dialect = info_commons_1.DIALECT;
    if (databaseId === "mock") {
        dialect = 'sqlite3';
    }
    if (dbClient == null) {
        const dbConfig = info_commons_1.config.get('db.' + databaseId);
        if (dbConfig == null) {
            throw new Error('Missing configuration for databaseId : ' + databaseId);
        }
        const knex = knex_config_1.knexClient(info_commons_1.config.get(constants_1.DB), databaseId, dialect);
        const dataSource = new datasource_1.KnexDatasource(knex);
        const dbEngine = constants_1.parseDBEngineEnum(dialect);
        dbClient = clients_1.createAppConfigDBClient(dbEngine, databaseId, dataSource);
        configDBCache.set(databaseId, dbClient);
    }
    return dbClient;
};
//# sourceMappingURL=metadata.util.js.map