"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var metadata_util_1 = require("./utilities/metadata.util");
exports.getAppConfigDBClient = metadata_util_1.getAppConfigDBClient;
var knex_config_1 = require("knex-config");
exports.knexClient = knex_config_1.knexClient;
var constants_1 = require("./constants");
exports.MOCK = constants_1.MOCK;
var info_commons_1 = require("info-commons");
exports.config = info_commons_1.config;
var models_1 = require("./models");
exports.ConfigMetadata = models_1.ConfigMetadata;
exports.ConfigItemModel = models_1.ConfigItemModel;
exports.ConfigItemProperty = models_1.ConfigItemProperty;
exports.ConfigItemRelation = models_1.ConfigItemRelation;
exports.ConfigItemPrivilege = models_1.ConfigItemPrivilege;
var init_config_1 = require("./init-config");
exports.MetaDBConfig = init_config_1.MetaDBConfig;
//# sourceMappingURL=index.js.map