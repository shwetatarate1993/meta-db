export { getAppConfigDBClient } from './utilities/metadata.util';
export { AppConfigDBClient } from './db/clients';
export { knexClient } from 'knex-config';
export { MOCK } from './constants';
export { config } from 'info-commons';
export { ConfigMetadata, ConfigItemModel, ConfigItemProperty, ConfigItemRelation, ConfigItemPrivilege, ConfigItem, } from './models';
export { MetaDBConfig } from './init-config';
//# sourceMappingURL=index.d.ts.map